# Slovak translation for gnome-keyring.
# Copyright (C) 2004, 2005, 2007, 2009, 2013 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-keyring package.
# Marcel Telka <marcel@telka.sk>, 2004, 2005.
# Peter Tuharsky <tuharsky@misbb.sk>, 2007.
# Mário Buči <mario.buci@gmail.com>, 2009.
# Dušan Kazik <prescott66@gmail.com>, 2013-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-keyring\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gcr/issues\n"
"POT-Creation-Date: 2021-09-30 14:31+0000\n"
"PO-Revision-Date: 2021-10-06 08:25+0200\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>\n"
"Language-Team: Slovak <gnome-sk-list@gnome.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0\n"
"X-Generator: Gtranslator 40.0\n"

#: egg/egg-oid.c:40
msgid "Domain Component"
msgstr "Komponent domény"

#: egg/egg-oid.c:42 ui/gcr-gnupg-renderer.c:410 ui/gcr-gnupg-renderer.c:579
msgid "User ID"
msgstr "Idetifikátor používateľa"

#: egg/egg-oid.c:45
msgid "Email Address"
msgstr "Emailová adresa"

#: egg/egg-oid.c:53
msgid "Date of Birth"
msgstr "Dátum narodenia"

#: egg/egg-oid.c:55
msgid "Place of Birth"
msgstr "Miesto narodenia"

#: egg/egg-oid.c:57
msgid "Gender"
msgstr "Pohlavie"

#: egg/egg-oid.c:59
msgid "Country of Citizenship"
msgstr "Štátna príslušnosť"

#: egg/egg-oid.c:61
msgid "Country of Residence"
msgstr "Krajina pobytu"

# Toto je v niektorých prípadoch prekladané ako Názov domény - ja osobnem by som nevedel čo mám zadať do poľa Bežný názov
# PŠ: http://www.alvestrand.no/objectid/2.5.4.3.html - objekt môže byť aj osoba (väčšinou je to potom v tvare "Meno Priezvisko")...
#: egg/egg-oid.c:64
msgid "Common Name"
msgstr "Bežný názov (CN)"

#: egg/egg-oid.c:66
msgid "Surname"
msgstr "Priezvisko"

#: egg/egg-oid.c:68 ui/gcr-certificate-renderer.c:565
msgid "Serial Number"
msgstr "Sériové číslo"

#: egg/egg-oid.c:70
msgid "Country"
msgstr "Krajina"

#: egg/egg-oid.c:72
msgid "Locality"
msgstr "Oblasť"

#: egg/egg-oid.c:74
msgid "State"
msgstr "Štát"

#: egg/egg-oid.c:76
msgid "Street"
msgstr "Ulica"

#: egg/egg-oid.c:78
msgid "Organization"
msgstr "Organizácia"

#: egg/egg-oid.c:80
msgid "Organizational Unit"
msgstr "Organizačná jednotka"

#: egg/egg-oid.c:82
msgid "Title"
msgstr "Titul"

#: egg/egg-oid.c:84
msgid "Telephone Number"
msgstr "Telefónne číslo"

#: egg/egg-oid.c:86
msgid "Given Name"
msgstr "Krstné meno"

#: egg/egg-oid.c:88
msgid "Initials"
msgstr "Iniciály"

#: egg/egg-oid.c:90
msgid "Generation Qualifier"
msgstr "Kvalifikátor generácie"

#: egg/egg-oid.c:92
msgid "DN Qualifier"
msgstr "Kvalifikátor DN"

#: egg/egg-oid.c:94
msgid "Pseudonym"
msgstr "Pseudonym"

#. Translators: Russian: Main state registration number
#: egg/egg-oid.c:98
msgid "OGRN"
msgstr "OGRN"

#. Translators: Russian: Individual insurance account number
#: egg/egg-oid.c:101
msgid "SNILS"
msgstr "SNILS"

#. Translators: Russian: Main state registration number for individual enterpreneurs
#: egg/egg-oid.c:104
msgid "OGRNIP"
msgstr "OGRNIP"

#. Translators: Russian: Individual taxpayer number
#: egg/egg-oid.c:107
msgid "INN"
msgstr "INN"

#: egg/egg-oid.c:110 ui/gcr-gnupg-renderer.c:201 ui/gcr-key-renderer.c:385
msgid "RSA"
msgstr "RSA"

#: egg/egg-oid.c:111
msgid "MD2 with RSA"
msgstr "MD2 s RSA"

#: egg/egg-oid.c:112
msgid "MD5 with RSA"
msgstr "MD5 s RSA"

#: egg/egg-oid.c:113
msgid "SHA1 with RSA"
msgstr "SHA1 s RSA"

#: egg/egg-oid.c:115 ui/gcr-gnupg-renderer.c:205 ui/gcr-key-renderer.c:387
msgid "DSA"
msgstr "DSA"

#: egg/egg-oid.c:116
msgid "SHA1 with DSA"
msgstr "SHA1 s DSA"

#: egg/egg-oid.c:118 ui/gcr-key-renderer.c:389
msgid "Elliptic Curve"
msgstr "Eliptická krivka"

#: egg/egg-oid.c:119
msgid "SHA1 with ECDSA"
msgstr "SHA1 s ECDSA"

#: egg/egg-oid.c:120
msgid "SHA224 with ECDSA"
msgstr "SHA224 s ECDSA"

#: egg/egg-oid.c:121
msgid "SHA256 with ECDSA"
msgstr "SHA256 s ECDSA"

#: egg/egg-oid.c:122
msgid "SHA384 with ECDSA"
msgstr "SHA384 s ECDSA"

#: egg/egg-oid.c:123
msgid "SHA512 with ECDSA"
msgstr "SHA512 s ECDSA"

#: egg/egg-oid.c:125
msgid "GOST R 34.11-94 with GOST R 34.10-2001"
msgstr "GOST R 34.11-94 s GOST R 34.10-2001"

#: egg/egg-oid.c:126
msgid "GOST R 34.10-2001"
msgstr "GOST R 34.10-2001"

#: egg/egg-oid.c:127
msgid "GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.10-2012 256-bitová krivka"

#: egg/egg-oid.c:128
msgid "GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.10-2012 512-bitová krivka"

#: egg/egg-oid.c:129
msgid "GOST R 34.11-2012/256 with GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.11-2012/256 s GOST R 34.10-2012 256-bitovou krivkou"

#: egg/egg-oid.c:130
msgid "GOST R 34.11-2012/512 with GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.11-2012/512 s GOST R 34.10-2012 512-bitovou krivkou"

#. Extended Key Usages
#: egg/egg-oid.c:133
msgid "Server Authentication"
msgstr "Overovanie totožnosti servera"

#: egg/egg-oid.c:134
msgid "Client Authentication"
msgstr "Overovanie totožnosti klienta"

#: egg/egg-oid.c:135
msgid "Code Signing"
msgstr "Podpisovanie kódu"

#: egg/egg-oid.c:136
msgid "Email Protection"
msgstr "Ochrana emailov"

#: egg/egg-oid.c:137
msgid "Time Stamping"
msgstr "Označovanie časovou značkou"

#: gck/gck-module.c:332
#, c-format
msgid "Error loading PKCS#11 module: %s"
msgstr "Chyba pri načítavaní modulu PKCS#11: %s"

#: gck/gck-module.c:346
#, c-format
msgid "Couldn’t initialize PKCS#11 module: %s"
msgstr "Nepodarilo sa inicializovať modul PKCS#11: %s"

#: gck/gck-modules.c:62
#, c-format
msgid "Couldn’t initialize registered PKCS#11 modules: %s"
msgstr "Nepodarilo sa inicializovať registrované moduly PKCS#11: %s"

#: gck/gck-uri.c:224
#, c-format
msgid "The URI has invalid encoding."
msgstr "Identifikátor URI obsahuje neplatné kódovanie."

#: gck/gck-uri.c:228
msgid "The URI does not have the “pkcs11” scheme."
msgstr "Identifikátor neobsahuje schému „pkcs11“."

#: gck/gck-uri.c:232
msgid "The URI has bad syntax."
msgstr "Identifikátor URI obsahuje zlú syntax."

#: gck/gck-uri.c:236
msgid "The URI has a bad version number."
msgstr "Identifikátor URI obsahuje nesprávne číslo verzie."

#: gcr/gcr-callback-output-stream.c:56 gcr/gcr-callback-output-stream.c:73
#, c-format
msgid "The stream was closed"
msgstr "Tok údajov bol zatvorený"

#. later
#. later
#: gcr/gcr-certificate.c:350 gcr/gcr-gnupg-key.c:429
msgctxt "column"
msgid "Name"
msgstr "Názov"

#: gcr/gcr-certificate.c:352
msgctxt "column"
msgid "Issued By"
msgstr "Vydal"

#. later
#: gcr/gcr-certificate.c:354
msgctxt "column"
msgid "Expires"
msgstr "Vyprší"

#: gcr/gcr-certificate.c:1186 gcr/gcr-parser.c:346
#: ui/gcr-certificate-renderer.c:103 ui/gcr-certificate-exporter.c:464
msgid "Certificate"
msgstr "Certifikát"

#: gcr/gcr-certificate-extensions.c:190
msgid "Other Name"
msgstr "Iný názov"

#: gcr/gcr-certificate-extensions.c:200
msgid "XMPP Addr"
msgstr "Adresa XMPP"

# DK: SRV = service record (SRV record)
#: gcr/gcr-certificate-extensions.c:204
msgid "DNS SRV"
msgstr "DNS SRV"

#: gcr/gcr-certificate-extensions.c:216 ui/gcr-gnupg-renderer.c:423
#: ui/gcr-gnupg-renderer.c:705
msgid "Email"
msgstr "E-mail"

#: gcr/gcr-certificate-extensions.c:224
msgid "DNS"
msgstr "DNS"

#: gcr/gcr-certificate-extensions.c:232
msgid "X400 Address"
msgstr "Adresa X400"

# MČ: tu som trochu mimo. Je to typu DNS, ale „dn“ evokuje LDAP.
#: gcr/gcr-certificate-extensions.c:239
msgid "Directory Name"
msgstr "Názov adresára"

# MČ: http://en.wikipedia.org/wiki/Electronic_data_interchange
#: gcr/gcr-certificate-extensions.c:247
msgid "EDI Party Name"
msgstr "Názov poskytovateľa EDI Party"

#: gcr/gcr-certificate-extensions.c:254
msgid "URI"
msgstr "URI"

#: gcr/gcr-certificate-extensions.c:262
msgid "IP Address"
msgstr "Adresa IP"

#: gcr/gcr-certificate-extensions.c:270
msgid "Registered ID"
msgstr "Registrovaný ID"

#: gcr/gcr-certificate-request.c:406
#, c-format
msgid "Unsupported key type for certificate request"
msgstr "Nepodporovaný typ kľúča pre žiadosť o certifikát"

#: gcr/gcr-certificate-request.c:493 gcr/gcr-certificate-request.c:577
#, c-format
msgid "The key cannot be used to sign the request"
msgstr "Kľúč sa nedá použiť na podpísanie požiadavky"

#: gcr/gcr-gnupg-importer.c:95
msgid "GnuPG Keyring"
msgstr "Zväzok kľúčov GnuPG"

#: gcr/gcr-gnupg-importer.c:97
#, c-format
msgid "GnuPG Keyring: %s"
msgstr "Zväzok kľúčov GnuPG: %s"

#: gcr/gcr-gnupg-key.c:143 gcr/gcr-parser.c:352 ui/gcr-gnupg-renderer.c:88
msgid "PGP Key"
msgstr "Kľúč PGP"

#: gcr/gcr-gnupg-key.c:431
msgctxt "column"
msgid "Key ID"
msgstr "ID kľúča"

#: gcr/gcr-gnupg-process.c:867
#, c-format
msgid "Gnupg process exited with code: %d"
msgstr "Proces Gnupg skončil s kódom: %d"

#: gcr/gcr-gnupg-process.c:874
#, c-format
msgid "Gnupg process was terminated with signal: %d"
msgstr "Proces Gnupg bol prerušený signálom: %d"

#: gcr/gcr-gnupg-process.c:928 gcr/gcr-parser.c:2598 gcr/gcr-parser.c:3192
#: gcr/gcr-system-prompt.c:932
msgid "The operation was cancelled"
msgstr "Operácia bola zrušená"

#: gcr/gcr-parser.c:343 ui/gcr-key-renderer.c:361
msgid "Private Key"
msgstr "Súkromný kľúč"

#: gcr/gcr-parser.c:349 ui/gcr-certificate-renderer.c:887
#: ui/gcr-gnupg-renderer.c:738 ui/gcr-key-renderer.c:370
msgid "Public Key"
msgstr "Verejný kľúč"

#: gcr/gcr-parser.c:355
msgid "Certificate Request"
msgstr "Požiadavka na certifikát"

#: gcr/gcr-parser.c:2601
msgid "Unrecognized or unsupported data."
msgstr "Nerozpoznané alebo nepodporované údaje."

#: gcr/gcr-parser.c:2604
msgid "Could not parse invalid or corrupted data."
msgstr "Nepodarilo sa analyzovať neplatné alebo porušené údaje."

#: gcr/gcr-parser.c:2607
msgid "The data is locked"
msgstr "Údaj je uzamknutý"

#: gcr/gcr-prompt.c:229
msgid "Continue"
msgstr "Pokračovať"

#: gcr/gcr-prompt.c:238
msgid "Cancel"
msgstr "Zrušiť"

#: gcr/gcr-ssh-agent-interaction.c:116
#, c-format
#| msgid "Unlock: %s"
msgid "Unlock password for: %s"
msgstr "Odomykacie heslo pre: %s"

#: gcr/gcr-ssh-agent-interaction.c:152
#| msgid "Private Key"
msgid "Unlock private key"
msgstr "Odomknutie súkromného kľúča"

#: gcr/gcr-ssh-agent-interaction.c:153
msgid "Enter password to unlock the private key"
msgstr "Zadajte heslo na odomknutie súkromného kľúča"

#. TRANSLATORS: The private key is locked
#: gcr/gcr-ssh-agent-interaction.c:156
#, c-format
msgid "An application wants access to the private key “%s”, but it is locked"
msgstr ""
"Aplikácia požaduje prístup k súkromnému kľúču „%s“, ten je ale uzamknutý"

# GtkRadioButton label
#: gcr/gcr-ssh-agent-interaction.c:161
#| msgid "Automatically unlock this keyring whenever I’m logged in"
msgid "Automatically unlock this key whenever I’m logged in"
msgstr "Automaticky odomknúť tento kľúč zakaždým keď sa prihlásim"

# GtkLabel label; button
#: gcr/gcr-ssh-agent-interaction.c:163 ui/gcr-pkcs11-import-dialog.ui:143
#: ui/gcr-unlock-renderer.c:70 ui/gcr-unlock-renderer.c:124
msgid "Unlock"
msgstr "Odomknúť"

#: gcr/gcr-ssh-agent-interaction.c:166
#| msgid "The password was incorrect"
msgid "The unlock password was incorrect"
msgstr "Odomykacie heslo nebolo správne"

#: gcr/gcr-ssh-agent-service.c:259
msgid "Unnamed"
msgstr "Bez názvu"

#: gcr/gcr-ssh-askpass.c:194
msgid "Enter your OpenSSH passphrase"
msgstr "Zadajte vaše OpenSSH heslo"

#: gcr/gcr-subject-public-key.c:405
msgid "Unrecognized or unavailable attributes for key"
msgstr "Nerozpoznané alebo nedostupné atribúty pre kľúč"

#: gcr/gcr-subject-public-key.c:491 gcr/gcr-subject-public-key.c:574
msgid "Couldn’t build public key"
msgstr "Nepodarilo sa zostaviť verejný kľúč"

#: gcr/gcr-system-prompt.c:912
msgid "Another prompt is already in progress"
msgstr "Už prebieha iná výzva"

#. Translators: A pinned certificate is an exception which
#. trusts a given certificate explicitly for a purpose and
#. communication with a certain peer.
#: gcr/gcr-trust.c:341
#, c-format
msgid "Couldn’t find a place to store the pinned certificate"
msgstr "Nepodarilo sa nájsť miesto na uloženie pripnutého certifikátu"

# heading
#: ui/gcr-certificate-renderer.c:118
msgid "Basic Constraints"
msgstr "Základné obmedzenia"

#: ui/gcr-certificate-renderer.c:120
msgid "Certificate Authority"
msgstr "Autorita certifikátu"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "Yes"
msgstr "Áno"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "No"
msgstr "Nie"

#: ui/gcr-certificate-renderer.c:124
msgid "Max Path Length"
msgstr "Maximálna dĺžka cesty"

#: ui/gcr-certificate-renderer.c:125
msgid "Unlimited"
msgstr "Neobmedzená"

# heading
#: ui/gcr-certificate-renderer.c:144
msgid "Extended Key Usage"
msgstr "Rozšírené využitie kľúča"

#: ui/gcr-certificate-renderer.c:155
msgid "Allowed Purposes"
msgstr "Povolené účely"

# heading
#: ui/gcr-certificate-renderer.c:175
msgid "Subject Key Identifier"
msgstr "Identifikátor kľúča subjektu"

#: ui/gcr-certificate-renderer.c:176
msgid "Key Identifier"
msgstr "Identifikátor kľúča"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:187
msgid "Digital signature"
msgstr "Digitálny podpis"

#: ui/gcr-certificate-renderer.c:188
msgid "Non repudiation"
msgstr "Nezavrhnutý"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:189
msgid "Key encipherment"
msgstr "Zašifrovanie kľúča"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:190
msgid "Data encipherment"
msgstr "Zašifrovanie údajov"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:191
msgid "Key agreement"
msgstr "Prijatie kľúča"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:192
msgid "Certificate signature"
msgstr "Podpis certifikátu"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:193
msgid "Revocation list signature"
msgstr "Podpis zoznamu odvolaní"

#: ui/gcr-certificate-renderer.c:194
msgid "Encipher only"
msgstr "Iba zašifrovanie"

# usage_descriptions
#: ui/gcr-certificate-renderer.c:195
msgid "Decipher only"
msgstr "Iba dešifrovanie"

# heading
#: ui/gcr-certificate-renderer.c:220
msgid "Key Usage"
msgstr "Využitie kľúča"

# MČ: množné číslo?
# PM: aj ked hodnot moze byt viac myslím ze to takto moze zostat
#: ui/gcr-certificate-renderer.c:221
msgid "Usages"
msgstr "Využitie"

# heading
#: ui/gcr-certificate-renderer.c:241
msgid "Subject Alternative Names"
msgstr "Alternatívne názvy subjektu"

# heading
#: ui/gcr-certificate-renderer.c:268
msgid "Extension"
msgstr "Rozšírenie"

#: ui/gcr-certificate-renderer.c:272
msgid "Identifier"
msgstr "Identifikátor"

#: ui/gcr-certificate-renderer.c:273 ui/gcr-certificate-request-renderer.c:268
#: ui/gcr-gnupg-renderer.c:414 ui/gcr-gnupg-renderer.c:431
msgid "Value"
msgstr "Hodnota"

#: ui/gcr-certificate-renderer.c:291
msgid "Couldn’t export the certificate."
msgstr "Nepodarilo sa exportovať certifikát."

#: ui/gcr-certificate-renderer.c:527 ui/gcr-certificate-request-renderer.c:309
msgid "Identity"
msgstr "Identita"

#: ui/gcr-certificate-renderer.c:531
msgid "Verified by"
msgstr "Overil"

#: ui/gcr-certificate-renderer.c:538 ui/gcr-gnupg-renderer.c:719
msgid "Expires"
msgstr "Vyprší"

#. The subject
#: ui/gcr-certificate-renderer.c:545 ui/gcr-certificate-request-renderer.c:315
msgid "Subject Name"
msgstr "Názov subjektu"

#. The Issuer
#: ui/gcr-certificate-renderer.c:550
msgid "Issuer Name"
msgstr "Názov vydavateľa"

#. The Issued Parameters
#: ui/gcr-certificate-renderer.c:555
msgid "Issued Certificate"
msgstr "Vydaný cerfikát"

#: ui/gcr-certificate-renderer.c:560 ui/gcr-certificate-request-renderer.c:326
msgid "Version"
msgstr "Verzia"

#: ui/gcr-certificate-renderer.c:574
msgid "Not Valid Before"
msgstr "Neplatný pred"

#: ui/gcr-certificate-renderer.c:579
msgid "Not Valid After"
msgstr "Neplatný po"

#. Fingerprints
#: ui/gcr-certificate-renderer.c:584
msgid "Certificate Fingerprints"
msgstr "Odtlačky certifikátu"

#. Public Key Info
#: ui/gcr-certificate-renderer.c:590 ui/gcr-certificate-request-renderer.c:329
#: ui/gcr-certificate-request-renderer.c:375
msgid "Public Key Info"
msgstr "Informácie o verejnom kľúči"

#. Signature
#: ui/gcr-certificate-renderer.c:605 ui/gcr-certificate-renderer.c:915
#: ui/gcr-certificate-request-renderer.c:345
#: ui/gcr-certificate-request-renderer.c:382 ui/gcr-gnupg-renderer.c:560
msgid "Signature"
msgstr "Podpis"

# menu item
#: ui/gcr-certificate-renderer.c:622
msgid "Export Certificate…"
msgstr "Exportovať certifikát…"

#: ui/gcr-certificate-renderer.c:861
msgid "Key Algorithm"
msgstr "Algoritmus kľúča"

#: ui/gcr-certificate-renderer.c:866
msgid "Key Parameters"
msgstr "Parametre kľúča"

#: ui/gcr-certificate-renderer.c:874 ui/gcr-gnupg-renderer.c:353
msgid "Key Size"
msgstr "Veľkosť kľúča"

#: ui/gcr-certificate-renderer.c:882
msgid "Key SHA1 Fingerprint"
msgstr "Odtlačok kľúča SHA1"

#: ui/gcr-certificate-renderer.c:904
msgid "Signature Algorithm"
msgstr "Algoritmus podpisu"

#: ui/gcr-certificate-renderer.c:908
msgid "Signature Parameters"
msgstr "Parametre podpisu"

# rozšírenie (áno/nie)
#: ui/gcr-certificate-renderer.c:957
msgid "Critical"
msgstr "Kritické"

#. The certificate request type
#: ui/gcr-certificate-request-renderer.c:95
#: ui/gcr-certificate-request-renderer.c:304
#: ui/gcr-certificate-request-renderer.c:319
#: ui/gcr-certificate-request-renderer.c:362
#: ui/gcr-certificate-request-renderer.c:367
msgid "Certificate request"
msgstr "Požiadavka na certifikát"

# heading
#: ui/gcr-certificate-request-renderer.c:257
msgid "Attribute"
msgstr "Atribút"

#: ui/gcr-certificate-request-renderer.c:261
#: ui/gcr-certificate-request-renderer.c:320
#: ui/gcr-certificate-request-renderer.c:368 ui/gcr-gnupg-renderer.c:591
#: ui/gcr-gnupg-renderer.c:593
msgid "Type"
msgstr "Typ"

#: ui/gcr-certificate-request-renderer.c:372
msgid "Challenge"
msgstr "Výzva"

# expander label
#: ui/gcr-display-view.c:298
msgid "_Details"
msgstr "_Podrobnosti"

#: ui/gcr-failure-renderer.c:159
#, c-format
msgid "Could not display “%s”"
msgstr "Nepodarilo sa zobraziť „%s“"

#: ui/gcr-failure-renderer.c:161
msgid "Could not display file"
msgstr "Nepodarilo sa zobraziť súbor"

#: ui/gcr-failure-renderer.c:166
msgid "Reason"
msgstr "Príčina"

#: ui/gcr-failure-renderer.c:216
#, c-format
msgid "Cannot display a file of this type."
msgstr "Nedá sa zobraziť súbor tohoto typu."

# algoritmus
#: ui/gcr-gnupg-renderer.c:203
msgid "Elgamal"
msgstr "Elgamal"

# Schopnosti
#: ui/gcr-gnupg-renderer.c:216
msgid "Encrypt"
msgstr "Zašifrovať"

# Schopnosti
#: ui/gcr-gnupg-renderer.c:218
msgid "Sign"
msgstr "Podpísať"

# Schopnosti
#: ui/gcr-gnupg-renderer.c:220
msgid "Certify"
msgstr "Certifikovať"

# Schopnosti
#: ui/gcr-gnupg-renderer.c:222
msgid "Authenticate"
msgstr "Overiť totožnosť"

# Schopnosti
# DK: https://bugzilla.gnome.org/show_bug.cgi?id=707077
#: ui/gcr-gnupg-renderer.c:224
msgctxt "capability"
msgid "Disabled"
msgstr "Zakázané"

# PM: Sedí rod vo všetkých prípadoch?
# MČ: myslím, že nesedí, prvé je ku Capabilities, druhe ku OwnerTrust, ďalšie ku typu kľúča a posledné ku Algoritmu.
# Dôvera vlastníka
#: ui/gcr-gnupg-renderer.c:255 ui/gcr-gnupg-renderer.c:414
#: ui/gcr-key-renderer.c:391 ui/gcr-key-renderer.c:395
msgid "Unknown"
msgstr "Neznáme"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:257
msgid "Invalid"
msgstr "Neplatná"

# Dôvera vlastníkovi
# DK: https://bugzilla.gnome.org/show_bug.cgi?id=707077
#: ui/gcr-gnupg-renderer.c:259
msgctxt "ownertrust"
msgid "Disabled"
msgstr "Zakázaná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:261
msgid "Revoked"
msgstr "Odvolaná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:263
msgid "Expired"
msgstr "Vypršaná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:265
msgid "Undefined trust"
msgstr "Nedefinovaná dôveryhodnosť"

# MČ: doteraz bol rod ženský. Tu začína mužský. Prečo?
# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:267
msgid "Distrusted"
msgstr "Nedôveryhodná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:269
msgid "Marginally trusted"
msgstr "Čiastočne dôveryhodná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:271
msgid "Fully trusted"
msgstr "Plne dôveryhodná"

# Dôvera vlastníkovi
#: ui/gcr-gnupg-renderer.c:273
msgid "Ultimately trusted"
msgstr "Výnimočne dôveryhodná"

# message
#: ui/gcr-gnupg-renderer.c:287
msgid "The information in this key has not yet been verified"
msgstr "Informácia v tomto kľúči ešte nebola overená"

# message
#: ui/gcr-gnupg-renderer.c:290
msgid "This key is invalid"
msgstr "Tento kľúč je neplatný"

# message
#: ui/gcr-gnupg-renderer.c:293
msgid "This key has been disabled"
msgstr "Tento kľúč bol zakázaný"

# message
#: ui/gcr-gnupg-renderer.c:296
msgid "This key has been revoked"
msgstr "Tento kľúč bol odvolaný"

# message
#: ui/gcr-gnupg-renderer.c:299
msgid "This key has expired"
msgstr "Tento kľúč vypršal"

# message
#: ui/gcr-gnupg-renderer.c:304
msgid "This key is distrusted"
msgstr "Tento kľúč je nedôveryhodný"

# message
#: ui/gcr-gnupg-renderer.c:307
msgid "This key is marginally trusted"
msgstr "Tento kľúč je čiastočne dôveryhodný"

# message
#: ui/gcr-gnupg-renderer.c:310
msgid "This key is fully trusted"
msgstr "Tento kľúč je plne dôveryhodný"

# message
#: ui/gcr-gnupg-renderer.c:313
msgid "This key is ultimately trusted"
msgstr "Tento kľúč je výnimočne dôveryhodný"

#: ui/gcr-gnupg-renderer.c:338 ui/gcr-gnupg-renderer.c:564
msgid "Key ID"
msgstr "ID kľúča"

#: ui/gcr-gnupg-renderer.c:346 ui/gcr-gnupg-renderer.c:572
#: ui/gcr-gnupg-renderer.c:619 ui/gcr-key-renderer.c:392
msgid "Algorithm"
msgstr "Algoritmus"

#: ui/gcr-gnupg-renderer.c:361 ui/gcr-gnupg-renderer.c:438
#: ui/gcr-gnupg-renderer.c:481
msgid "Created"
msgstr "Vytvorený"

#: ui/gcr-gnupg-renderer.c:370 ui/gcr-gnupg-renderer.c:447
#: ui/gcr-gnupg-renderer.c:490
msgid "Expiry"
msgstr "Vyprší"

#: ui/gcr-gnupg-renderer.c:379
msgid "Capabilities"
msgstr "Schopnosti"

#: ui/gcr-gnupg-renderer.c:392
msgid "Owner trust"
msgstr "Dôvera vlastníkovi"

#: ui/gcr-gnupg-renderer.c:420
msgid "Name"
msgstr "Meno"

#: ui/gcr-gnupg-renderer.c:426 ui/gcr-gnupg-renderer.c:708
msgid "Comment"
msgstr "Komentár"

# heading
#: ui/gcr-gnupg-renderer.c:466
msgid "User Attribute"
msgstr "Atribút používateľa"

#: ui/gcr-gnupg-renderer.c:473 ui/gcr-key-renderer.c:398
msgid "Size"
msgstr "Veľkosť"

# Trieda
#: ui/gcr-gnupg-renderer.c:508
msgid "Signature of a binary document"
msgstr "Podpis binárneho dokumentu"

# Trieda
#: ui/gcr-gnupg-renderer.c:510
msgid "Signature of a canonical text document"
msgstr "Podpis kanonického textového dokumentu"

# Trieda
#: ui/gcr-gnupg-renderer.c:512
msgid "Standalone signature"
msgstr "Samostatný podpis"

# Trieda
#: ui/gcr-gnupg-renderer.c:514
msgid "Generic certification of key"
msgstr "Všeobecná certifikácia kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:516
msgid "Persona certification of key"
msgstr "Osobná certifikácia kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:518
msgid "Casual certification of key"
msgstr "Príležitostná certifikácia kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:520
msgid "Positive certification of key"
msgstr "Pozitívna certifikácia kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:522
msgid "Subkey binding signature"
msgstr "Podpis spojenia podkľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:524
msgid "Primary key binding signature"
msgstr "Podpis primárneho spojenia kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:526
msgid "Signature directly on key"
msgstr "Podpis priamo v kľúči"

# Trieda
#: ui/gcr-gnupg-renderer.c:528
msgid "Key revocation signature"
msgstr "Podpis pre odvolanie kľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:530
msgid "Subkey revocation signature"
msgstr "Podpis pre odvolanie podkľúča"

# Trieda
#: ui/gcr-gnupg-renderer.c:532
msgid "Certification revocation signature"
msgstr "Podpis pre odvolanie certifikácie"

# Trieda
#: ui/gcr-gnupg-renderer.c:534
msgid "Timestamp signature"
msgstr "Podpis časovej značky"

# Trieda
#: ui/gcr-gnupg-renderer.c:536
msgid "Third-party confirmation signature"
msgstr "Podpis potvrdenia tretích strán"

#: ui/gcr-gnupg-renderer.c:589 ui/gcr-gnupg-renderer.c:597
msgid "Class"
msgstr "Trieda"

# Typ
#: ui/gcr-gnupg-renderer.c:591
msgid "Local only"
msgstr "Iba lokálny"

# Typ
#: ui/gcr-gnupg-renderer.c:593
msgid "Exportable"
msgstr "Exportovateľný"

# heading
#: ui/gcr-gnupg-renderer.c:611
msgid "Revocation Key"
msgstr "Kľúč pre odvolanie"

#: ui/gcr-gnupg-renderer.c:625 ui/gcr-gnupg-renderer.c:649
#: ui/gcr-gnupg-renderer.c:651
msgid "Fingerprint"
msgstr "Odtlačok"

#: ui/gcr-gnupg-renderer.c:740
msgid "Public Subkey"
msgstr "Verejný podkľúč"

#: ui/gcr-gnupg-renderer.c:742
msgid "Secret Key"
msgstr "Tajný kľúč"

#: ui/gcr-gnupg-renderer.c:744
msgid "Secret Subkey"
msgstr "Tajný podkľúč"

# DK: https://bugzilla.gnome.org/show_bug.cgi?id=707078
# spinner tooltip
#: ui/gcr-import-button.c:118
msgid "Initializing…"
msgstr "Inicializuje sa…"

# DK: https://bugzilla.gnome.org/show_bug.cgi?id=707078
# spinner tooltip
#: ui/gcr-import-button.c:126
msgid "Import is in progress…"
msgstr "Prebieha importovanie…"

# spinner tooltip
#: ui/gcr-import-button.c:133
#, c-format
msgid "Imported to: %s"
msgstr "Importované do: %s"

# spinner tooltip
#: ui/gcr-import-button.c:153
#, c-format
msgid "Import to: %s"
msgstr "Importovať do: %s"

# spinner tooltip
#: ui/gcr-import-button.c:166
msgid "Cannot import because there are no compatible importers"
msgstr ""
"Nedá sa importovať, pretože nie sú dostupné žiadne kompatibilné nástroje na "
"importovanie"

# spinner tooltip
#: ui/gcr-import-button.c:175
msgid "No data to import"
msgstr "Žiadne údaje na importovanie"

#: ui/gcr-key-renderer.c:89
msgid "Key"
msgstr "Kľúč"

#: ui/gcr-key-renderer.c:355
msgid "Private RSA Key"
msgstr "Súkromný kľúč RSA"

#: ui/gcr-key-renderer.c:357
msgid "Private DSA Key"
msgstr "Súkromný kľúč DSA"

#: ui/gcr-key-renderer.c:359
msgid "Private Elliptic Curve Key"
msgstr "Kľúč súkromnej eliptickej krivky"

#: ui/gcr-key-renderer.c:364 ui/gcr-key-renderer.c:366
msgid "Public DSA Key"
msgstr "Verejný kľúč DSA"

#: ui/gcr-key-renderer.c:368
msgid "Public Elliptic Curve Key"
msgstr "Kľúč verejnej eliptickej krivky"

# Sila
#: ui/gcr-key-renderer.c:377
#, c-format
msgid "%u bit"
msgid_plural "%u bits"
msgstr[0] "%u bitov"
msgstr[1] "%u bit"
msgstr[2] "%u bity"

#: ui/gcr-key-renderer.c:378
msgid "Strength"
msgstr "Sila"

#. Fingerprints
#: ui/gcr-key-renderer.c:402
msgid "Fingerprints"
msgstr "Odtlačky"

#: ui/gcr-key-renderer.c:406
msgid "SHA1"
msgstr "SHA1"

#: ui/gcr-key-renderer.c:411
msgid "SHA256"
msgstr "SHA256"

#. Add our various buttons
#: ui/gcr-pkcs11-import-dialog.c:104 ui/gcr-prompt-dialog.c:605
#: ui/gcr-certificate-exporter.c:229 ui/gcr-certificate-exporter.c:306
msgid "_Cancel"
msgstr "_Zrušiť"

#: ui/gcr-pkcs11-import-dialog.c:106 ui/gcr-prompt-dialog.c:608
msgid "_OK"
msgstr "_OK"

#: ui/gcr-pkcs11-import-dialog.c:179
msgid "Automatically chosen"
msgstr "Automaticky zvolený"

#: ui/gcr-pkcs11-import-dialog.c:263 ui/gcr-pkcs11-import-interaction.c:142
#: ui/gcr-pkcs11-import-interaction.c:161
#, c-format
msgid "The user cancelled the operation"
msgstr "Používateľ zrušil operáciu"

# GtkLabel label
#: ui/gcr-pkcs11-import-dialog.ui:31
msgid "In order to import, please enter the password."
msgstr "Ak chcete vykonať importovanie, prosím, zadajte heslo."

#. The password label
#: ui/gcr-pkcs11-import-dialog.ui:66 ui/gcr-prompt-dialog.c:666
msgid "Password:"
msgstr "Heslo:"

# GtkLabel label
#: ui/gcr-pkcs11-import-dialog.ui:80
msgid "Token:"
msgstr "Token:"

# GtkLabel label
#: ui/gcr-pkcs11-import-dialog.ui:178
msgid "Label:"
msgstr "Označenie:"

# GtkLabel label
#: ui/gcr-pkcs11-import-dialog.ui:233
msgid "Import settings"
msgstr "Importovať nastavenia"

#. The confirm label
#: ui/gcr-prompt-dialog.c:683
msgid "Confirm:"
msgstr "Potvrdenie:"

#: ui/gcr-prompt-dialog.c:751
msgid "Passwords do not match."
msgstr "Heslá nie sú zhodné."

#: ui/gcr-prompt-dialog.c:758
msgid "Password cannot be blank"
msgstr "Heslo nemôže byť prázdne"

# desktop entry name
#: ui/gcr-prompter.desktop.in.in:3
msgid "Access Prompt"
msgstr "Výzva na prístup"

# desktop entry comment
#: ui/gcr-prompter.desktop.in.in:4
msgid "Unlock access to passwords and other secrets"
msgstr "Odomkne prístup k heslám a iným tajnostiam"

#: ui/gcr-certificate-exporter.c:226
msgid "A file already exists with this name."
msgstr "Súbor s týmto názvom už existuje."

#: ui/gcr-certificate-exporter.c:227
msgid "Do you want to replace it with a new file?"
msgstr "Chcete ho nahradiť novým súborom?"

# button
#: ui/gcr-certificate-exporter.c:230
msgid "_Replace"
msgstr "_Nahradiť"

#: ui/gcr-certificate-exporter.c:260
#, c-format
msgid "The operation was cancelled."
msgstr "Operácia bola zrušená."

# file chooser dialog title
#: ui/gcr-certificate-exporter.c:304
msgid "Export certificate"
msgstr "Export certifikátu"

#: ui/gcr-certificate-exporter.c:307
msgid "_Save"
msgstr "_Uložiť"

# file filter
#: ui/gcr-certificate-exporter.c:316
msgid "Certificate files"
msgstr "Súbory certifikátu"

# file filter
#: ui/gcr-certificate-exporter.c:327
msgid "PEM files"
msgstr "Súbory PEM"

# GtkRadioButton label
#: ui/gcr-unlock-options-widget.ui:16
msgid "Automatically unlock this keyring whenever I’m logged in"
msgstr "Automaticky odomknúť tento zväzok kľúčov, keď sa prihlásim."

# GtkRadioButton label
#: ui/gcr-unlock-options-widget.ui:31
msgid "Lock this keyring when I log out"
msgstr "Uzamknúť tento zväzok kľúčov, keď sa odhlásim."

# GtkRadioButton label
#: ui/gcr-unlock-options-widget.ui:53
msgid "Lock this keyring after"
msgstr "Uzamknúť tento zväzok kľúčov po"

# GtkRadioButton label
#: ui/gcr-unlock-options-widget.ui:67
msgid "Lock this keyring if idle for"
msgstr "Uzamknúť tento zväzok kľúčov pri nečinnosti po"

# PM: tu by sa hodil ten plural forms ale typujem že nam ho sem nebudú vedieť urobiť
#. Translators: The 'minutes' from 'Lock this keyring if idle for x minutes'.
#: ui/gcr-unlock-options-widget.ui:103
msgid "minutes"
msgstr "minútach"

#: ui/gcr-unlock-renderer.c:68
#, c-format
msgid "Unlock: %s"
msgstr "Odomknúť: %s"

#: ui/gcr-unlock-renderer.c:122
msgid "Password"
msgstr "Heslo"

#: ui/gcr-unlock-renderer.c:274
#, c-format
msgid ""
"The contents of “%s” are locked. In order to view the contents, enter the "
"correct password."
msgstr ""
"Obsah „%s“ je uzamknutý. Ak chcete tento obsah zobraziť, zadajte správne "
"heslo."

#: ui/gcr-unlock-renderer.c:277
msgid ""
"The contents are locked. In order to view the contents, enter the correct "
"password."
msgstr ""
"Obsah je uzamknutý. Ak chcete tento obsah zobraziť, zadajte správne heslo."

#: ui/gcr-viewer.desktop.in.in:3
msgid "View file"
msgstr "Zobraziť súbor"

# PM: asi GCR - Prehliadač ...
#: ui/gcr-viewer-tool.c:40
msgid "GCR Certificate and Key Viewer"
msgstr "GCR - Prehliadač certifikátov a kľúčov"

# cmd desc
#: ui/gcr-viewer-tool.c:47
msgid "Show the application's version"
msgstr "Zobrazí verziu aplikácie"

# MČ: tu by trojbodka byť nemala, lebo sa to vypisuje do terminálu kde sú znaky rovnakej šírky. Tak nejako bola odpoveď v bugzille.
# DK: https://bugzilla.gnome.org/show_bug.cgi?id=707078
#: ui/gcr-viewer-tool.c:49
msgid "[file...]"
msgstr "[súbor...]"

#: ui/gcr-viewer-tool.c:100
msgid "- View certificate and key files"
msgstr "- Zobrazí súbory certifikátov a kľúčov"

#: ui/gcr-viewer-tool.c:114 ui/gcr-viewer-widget.c:669
msgid "Certificate Viewer"
msgstr "Prehliadač certifikátov"

#: ui/gcr-viewer-widget.c:189
msgid "The password was incorrect"
msgstr "Heslo bolo nesprávne"

#: ui/gcr-viewer-window.c:74
msgid "Imported"
msgstr "Importované"

#: ui/gcr-viewer-window.c:78
msgid "Import failed"
msgstr "Importovanie zlyhalo"

#: ui/gcr-viewer-window.c:105
msgid "Import"
msgstr "Importovať"

#: ui/gcr-viewer-window.c:114
msgid "_Close"
msgstr "_Zavrieť"
