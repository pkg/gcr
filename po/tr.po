# Turkish translation of gnome-keyring.
# Copyright (C) 2004 THE gnome-keyring'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-keyring package.
#
#
# Gorkem Cetin <gorkem@bahcesehir.edu.tr>, 2004.
# Görkem Çetin <gorkem@gorkemcetin.com>, 2004.
# Serdar CICEK <serdar@nerd.com.tr>, 2008.
# Baris Cicek <baris@teamforce.name.tr>, 2008, 2009.
# Necdet Yücel <necdetyucel@gmail.com>, 2015.
# Simge Sezgin <simgesezgin88@gmail.com>, 2015.
# Muhammet Kara <muhammetk@gmail.com>, 2011, 2014, 2015, 2018.
# Burhan Keleş <klsburhan@hotmail.com>, 2021.
# Emin Tufan Çetin <etcetin@gmail.com>, 2017, 2018, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-keyring\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gcr/issues\n"
"POT-Creation-Date: 2021-07-13 19:42+0000\n"
"PO-Revision-Date: 2021-09-19 13:34+0300\n"
"Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>\n"
"Language-Team: Türkçe <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.0\n"
"X-POOTLE-MTIME: 1420669651.000000\n"

#: egg/egg-oid.c:40
msgid "Domain Component"
msgstr "Alan Bileşeni"

#: egg/egg-oid.c:42 ui/gcr-gnupg-renderer.c:410 ui/gcr-gnupg-renderer.c:579
msgid "User ID"
msgstr "Kullanıcı Kimliği"

#: egg/egg-oid.c:45
msgid "Email Address"
msgstr "E-posta Adresi"

#: egg/egg-oid.c:53
msgid "Date of Birth"
msgstr "Doğum Tarihi"

#: egg/egg-oid.c:55
msgid "Place of Birth"
msgstr "Doğum Yeri"

#: egg/egg-oid.c:57
msgid "Gender"
msgstr "Cinsiyet"

#: egg/egg-oid.c:59
msgid "Country of Citizenship"
msgstr "Vatandaşlık Ülkesi"

#: egg/egg-oid.c:61
msgid "Country of Residence"
msgstr "Yerleşim Ülkesi"

#: egg/egg-oid.c:64
msgid "Common Name"
msgstr "Ortak Ad"

#: egg/egg-oid.c:66
msgid "Surname"
msgstr "Soyadı"

#: egg/egg-oid.c:68 ui/gcr-certificate-renderer.c:565
msgid "Serial Number"
msgstr "Seri Numarası"

#: egg/egg-oid.c:70
msgid "Country"
msgstr "Ülke"

#: egg/egg-oid.c:72
msgid "Locality"
msgstr "Yöre"

#: egg/egg-oid.c:74
msgid "State"
msgstr "Eyalet"

#: egg/egg-oid.c:76
msgid "Street"
msgstr "Sokak"

#: egg/egg-oid.c:78
msgid "Organization"
msgstr "Kurum"

#: egg/egg-oid.c:80
msgid "Organizational Unit"
msgstr "Kurum Birimi"

#: egg/egg-oid.c:82
msgid "Title"
msgstr "Başlık"

#: egg/egg-oid.c:84
msgid "Telephone Number"
msgstr "Telefon Numarası"

#: egg/egg-oid.c:86
msgid "Given Name"
msgstr "Verilen Ad"

#: egg/egg-oid.c:88
msgid "Initials"
msgstr "Başharfleri"

#: egg/egg-oid.c:90
msgid "Generation Qualifier"
msgstr "Üretim Belirteci"

#: egg/egg-oid.c:92
msgid "DN Qualifier"
msgstr "DN Belirteci"

#: egg/egg-oid.c:94
msgid "Pseudonym"
msgstr "Takma Ad"

#. Translators: Russian: Main state registration number
#: egg/egg-oid.c:98
msgid "OGRN"
msgstr "OGRN"

#. Translators: Russian: Individual insurance account number
#: egg/egg-oid.c:101
msgid "SNILS"
msgstr "SNILS"

#. Translators: Russian: Main state registration number for individual enterpreneurs
#: egg/egg-oid.c:104
msgid "OGRNIP"
msgstr "OGRNIP"

#. Translators: Russian: Individual taxpayer number
#: egg/egg-oid.c:107
msgid "INN"
msgstr "INN"

#: egg/egg-oid.c:110 ui/gcr-gnupg-renderer.c:201 ui/gcr-key-renderer.c:385
msgid "RSA"
msgstr "RSA"

#: egg/egg-oid.c:111
msgid "MD2 with RSA"
msgstr "RSA ile MD2"

#: egg/egg-oid.c:112
msgid "MD5 with RSA"
msgstr "RSA ile MD5"

#: egg/egg-oid.c:113
msgid "SHA1 with RSA"
msgstr "RSA ile SHA1"

#: egg/egg-oid.c:115 ui/gcr-gnupg-renderer.c:205 ui/gcr-key-renderer.c:387
msgid "DSA"
msgstr "DSA"

#: egg/egg-oid.c:116
msgid "SHA1 with DSA"
msgstr "DSA ile SHA1"

#: egg/egg-oid.c:118 ui/gcr-key-renderer.c:389
msgid "Elliptic Curve"
msgstr "Eliptik Eğri"

#: egg/egg-oid.c:119
msgid "SHA1 with ECDSA"
msgstr "ECDSA ile SHA1"

#: egg/egg-oid.c:120
msgid "SHA224 with ECDSA"
msgstr "ECDSA ile SHA224"

#: egg/egg-oid.c:121
msgid "SHA256 with ECDSA"
msgstr "ECDSA ile SHA256"

#: egg/egg-oid.c:122
msgid "SHA384 with ECDSA"
msgstr "ECDSA ile SHA384"

#: egg/egg-oid.c:123
msgid "SHA512 with ECDSA"
msgstr "ECDSA ile SHA512"

#: egg/egg-oid.c:125
msgid "GOST R 34.11-94 with GOST R 34.10-2001"
msgstr "GOST R 34.10-2001 ile GOST R 34.11-94"

#: egg/egg-oid.c:126
msgid "GOST R 34.10-2001"
msgstr "GOST R 34.10-2001"

#: egg/egg-oid.c:127
msgid "GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.10-2012 256-bit eğri"

#: egg/egg-oid.c:128
msgid "GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.10-2012 512-bit eğri"

#: egg/egg-oid.c:129
msgid "GOST R 34.11-2012/256 with GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.10-2012 256-bit eğri ile GOST R 34.11-2012/256"

#: egg/egg-oid.c:130
msgid "GOST R 34.11-2012/512 with GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.10-2012 512-bit eğri ile GOST R 34.11-2012/512"

#. Extended Key Usages
#: egg/egg-oid.c:133
msgid "Server Authentication"
msgstr "Sunucu Kimlik Denetimi"

#: egg/egg-oid.c:134
msgid "Client Authentication"
msgstr "İstemci Kimlik Denetimi"

#: egg/egg-oid.c:135
msgid "Code Signing"
msgstr "Kod İmzalama"

#: egg/egg-oid.c:136
msgid "Email Protection"
msgstr "E-posta Koruma"

#: egg/egg-oid.c:137
msgid "Time Stamping"
msgstr "Zaman Damgalama"

#: gck/gck-module.c:332
#, c-format
msgid "Error loading PKCS#11 module: %s"
msgstr "PKCS#11 modulü yüklemede hata: %s"

#: gck/gck-module.c:346
#, c-format
msgid "Couldn’t initialize PKCS#11 module: %s"
msgstr "PKCS#11 modülü başlatılamadı: %s"

#: gck/gck-modules.c:62
#, c-format
msgid "Couldn’t initialize registered PKCS#11 modules: %s"
msgstr "Kayıtlı PKCS#11 modülleri başlatılamadı: %s"

#: gck/gck-uri.c:224
#, c-format
msgid "The URI has invalid encoding."
msgstr "URI geçersiz kodlamaya sahip."

#: gck/gck-uri.c:228
msgid "The URI does not have the “pkcs11” scheme."
msgstr "URI, “pkcs11” şemasına sahip değil."

#: gck/gck-uri.c:232
msgid "The URI has bad syntax."
msgstr "URI kötü sözdizimine sahip."

#: gck/gck-uri.c:236
msgid "The URI has a bad version number."
msgstr "URI kötü bir sürüm numarasına sahip."

#: gcr/gcr-callback-output-stream.c:56 gcr/gcr-callback-output-stream.c:73
#, c-format
msgid "The stream was closed"
msgstr "Akış kapatıldı"

#. later
#. later
#: gcr/gcr-certificate.c:350 gcr/gcr-gnupg-key.c:429
msgctxt "column"
msgid "Name"
msgstr "Ad"

#: gcr/gcr-certificate.c:352
msgctxt "column"
msgid "Issued By"
msgstr "Sertifika Veren"

#. later
#: gcr/gcr-certificate.c:354
msgctxt "column"
msgid "Expires"
msgstr "Bitiş Tarihi"

#: gcr/gcr-certificate.c:1186 gcr/gcr-parser.c:346
#: ui/gcr-certificate-renderer.c:103 ui/gcr-certificate-exporter.c:464
msgid "Certificate"
msgstr "Sertifika"

#: gcr/gcr-certificate-extensions.c:190
msgid "Other Name"
msgstr "Diğer Ad"

#: gcr/gcr-certificate-extensions.c:200
msgid "XMPP Addr"
msgstr "XMPP Adresi"

#: gcr/gcr-certificate-extensions.c:204
msgid "DNS SRV"
msgstr "DNS Sunucu"

#: gcr/gcr-certificate-extensions.c:216 ui/gcr-gnupg-renderer.c:423
#: ui/gcr-gnupg-renderer.c:705
msgid "Email"
msgstr "E-posta"

#: gcr/gcr-certificate-extensions.c:224
msgid "DNS"
msgstr "DNS"

#: gcr/gcr-certificate-extensions.c:232
msgid "X400 Address"
msgstr "X400 Adresi"

#: gcr/gcr-certificate-extensions.c:239
msgid "Directory Name"
msgstr "Dizin Adı"

#: gcr/gcr-certificate-extensions.c:247
msgid "EDI Party Name"
msgstr "EDI Birim Adı"

#: gcr/gcr-certificate-extensions.c:254
msgid "URI"
msgstr "URI"

#: gcr/gcr-certificate-extensions.c:262
msgid "IP Address"
msgstr "IP Adresi"

#: gcr/gcr-certificate-extensions.c:270
msgid "Registered ID"
msgstr "Kayıtlı Kimlik"

#: gcr/gcr-certificate-request.c:406
#, c-format
msgid "Unsupported key type for certificate request"
msgstr "Sertifika isteği için desteklenmeyen anahtar türü"

#: gcr/gcr-certificate-request.c:493 gcr/gcr-certificate-request.c:577
#, c-format
msgid "The key cannot be used to sign the request"
msgstr "Anahtar isteği imzalamak için kullanılabilir değildir"

#: gcr/gcr-gnupg-importer.c:95
msgid "GnuPG Keyring"
msgstr "GnuPG Anahtarlığı"

#: gcr/gcr-gnupg-importer.c:97
#, c-format
msgid "GnuPG Keyring: %s"
msgstr "GnuPG Anahtarlığı: %s"

#: gcr/gcr-gnupg-key.c:143 gcr/gcr-parser.c:352 ui/gcr-gnupg-renderer.c:88
msgid "PGP Key"
msgstr "PGP Anahtarı"

#: gcr/gcr-gnupg-key.c:431
msgctxt "column"
msgid "Key ID"
msgstr "Anahtar Kimliği"

#: gcr/gcr-gnupg-process.c:867
#, c-format
msgid "Gnupg process exited with code: %d"
msgstr "Gnupg süreci şu kod ile sonuçlandı: %d"

#: gcr/gcr-gnupg-process.c:874
#, c-format
msgid "Gnupg process was terminated with signal: %d"
msgstr "Gnupg süreci sinyal ile sonlandırıldı: %d"

#: gcr/gcr-gnupg-process.c:928 gcr/gcr-parser.c:2598 gcr/gcr-parser.c:3192
#: gcr/gcr-system-prompt.c:932
msgid "The operation was cancelled"
msgstr "İşlem iptal edildi"

#: gcr/gcr-parser.c:343 ui/gcr-key-renderer.c:361
msgid "Private Key"
msgstr "Özel Anahtar"

#: gcr/gcr-parser.c:349 ui/gcr-certificate-renderer.c:887
#: ui/gcr-gnupg-renderer.c:738 ui/gcr-key-renderer.c:370
msgid "Public Key"
msgstr "Genel Anahtar"

#: gcr/gcr-parser.c:355
msgid "Certificate Request"
msgstr "Sertifika İsteği"

#: gcr/gcr-parser.c:2601
msgid "Unrecognized or unsupported data."
msgstr "Tanınmayan veya desteklenmeyen veri."

#: gcr/gcr-parser.c:2604
msgid "Could not parse invalid or corrupted data."
msgstr "Geçersiz veya bozuk veri çözümlenemedi."

#: gcr/gcr-parser.c:2607
msgid "The data is locked"
msgstr "Veri kilitli"

#: gcr/gcr-prompt.c:229
msgid "Continue"
msgstr "Sürdür"

#: gcr/gcr-prompt.c:238
msgid "Cancel"
msgstr "İptal"

#: gcr/gcr-ssh-agent-interaction.c:116
#, c-format
msgid "Unlock password for: %s"
msgstr "Şunun için kilit açma şifresi: %s"

#: gcr/gcr-ssh-agent-interaction.c:152
msgid "Unlock private key"
msgstr "Özel anahtar kilidini aç"

#: gcr/gcr-ssh-agent-interaction.c:153
msgid "Enter password to unlock the private key"
msgstr "Özel anahtar kilidini açmak için parolayı gir"

#. TRANSLATORS: The private key is locked
#: gcr/gcr-ssh-agent-interaction.c:156
#, c-format
msgid "An application wants access to the private key “%s”, but it is locked"
msgstr ""
"Bir uygulama “%s” özel anahtarına erişmek istiyor ancak anahtar kilitli"

#: gcr/gcr-ssh-agent-interaction.c:161
msgid "Automatically unlock this key whenever I’m logged in"
msgstr "Her oturum açtığımda bu anahtarın kilidini kendiliğinden aç"

#: gcr/gcr-ssh-agent-interaction.c:163 ui/gcr-pkcs11-import-dialog.ui:143
#: ui/gcr-unlock-renderer.c:70 ui/gcr-unlock-renderer.c:124
msgid "Unlock"
msgstr "Kilidi aç"

#: gcr/gcr-ssh-agent-interaction.c:166
msgid "The unlock password was incorrect"
msgstr "Kilit açma parolası yanlıştı"

#: gcr/gcr-ssh-agent-service.c:259
msgid "Unnamed"
msgstr "Adsız"

#: gcr/gcr-ssh-askpass.c:194
msgid "Enter your OpenSSH passphrase"
msgstr "OpenSSH parolanızı giriniz"

#: gcr/gcr-subject-public-key.c:405
msgid "Unrecognized or unavailable attributes for key"
msgstr "Anahtar için kullanılamaz ya da onaylanmamış öznitelikler"

#: gcr/gcr-subject-public-key.c:491 gcr/gcr-subject-public-key.c:574
msgid "Couldn’t build public key"
msgstr "Genel anahtar oluşturulamadı"

#: gcr/gcr-system-prompt.c:912
msgid "Another prompt is already in progress"
msgstr "Başka istek zaten sürüyor"

#. Translators: A pinned certificate is an exception which
#. trusts a given certificate explicitly for a purpose and
#. communication with a certain peer.
#: gcr/gcr-trust.c:341
#, c-format
msgid "Couldn’t find a place to store the pinned certificate"
msgstr "Tutturulmuş sertifikayı saklayacak konum bulunamadı"

#: ui/gcr-certificate-renderer.c:118
msgid "Basic Constraints"
msgstr "Temel Kısıtlar"

#: ui/gcr-certificate-renderer.c:120
msgid "Certificate Authority"
msgstr "Sertifika Yetkilisi"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "Yes"
msgstr "Evet"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "No"
msgstr "Hayır"

#: ui/gcr-certificate-renderer.c:124
msgid "Max Path Length"
msgstr "Azami Yol Uzunluğu"

#: ui/gcr-certificate-renderer.c:125
msgid "Unlimited"
msgstr "Sınırsız"

#: ui/gcr-certificate-renderer.c:144
msgid "Extended Key Usage"
msgstr "Uzatılmış Anahtar Kullanımı"

#: ui/gcr-certificate-renderer.c:155
msgid "Allowed Purposes"
msgstr "İzin Verilen Amaçlar"

#: ui/gcr-certificate-renderer.c:175
msgid "Subject Key Identifier"
msgstr "Konu Anahtar Tanımlayıcısı"

#: ui/gcr-certificate-renderer.c:176
msgid "Key Identifier"
msgstr "Anahtar Tanımlayıcı"

#: ui/gcr-certificate-renderer.c:187
msgid "Digital signature"
msgstr "Sayısal imza"

#: ui/gcr-certificate-renderer.c:188
msgid "Non repudiation"
msgstr "Red olmayan"

#: ui/gcr-certificate-renderer.c:189
msgid "Key encipherment"
msgstr "Anahtar şifreleme"

#: ui/gcr-certificate-renderer.c:190
msgid "Data encipherment"
msgstr "Veri şifreleme"

#: ui/gcr-certificate-renderer.c:191
msgid "Key agreement"
msgstr "Anahtar anlaşması"

#: ui/gcr-certificate-renderer.c:192
msgid "Certificate signature"
msgstr "Sertifika imzası"

#: ui/gcr-certificate-renderer.c:193
msgid "Revocation list signature"
msgstr "İptal Listesi İmzası"

#: ui/gcr-certificate-renderer.c:194
msgid "Encipher only"
msgstr "Yalnızca şifreleme"

#: ui/gcr-certificate-renderer.c:195
msgid "Decipher only"
msgstr "Yalnızca şifre çözme"

#: ui/gcr-certificate-renderer.c:220
msgid "Key Usage"
msgstr "Anahtar Kullanımı"

#: ui/gcr-certificate-renderer.c:221
msgid "Usages"
msgstr "Kullanımlar"

#: ui/gcr-certificate-renderer.c:241
msgid "Subject Alternative Names"
msgstr "Konu Diğer Adları"

#: ui/gcr-certificate-renderer.c:268
msgid "Extension"
msgstr "Uzantı"

#: ui/gcr-certificate-renderer.c:272
msgid "Identifier"
msgstr "Tanımlayıcı"

#: ui/gcr-certificate-renderer.c:273 ui/gcr-certificate-request-renderer.c:268
#: ui/gcr-gnupg-renderer.c:414 ui/gcr-gnupg-renderer.c:431
msgid "Value"
msgstr "Değer"

#: ui/gcr-certificate-renderer.c:291
msgid "Couldn’t export the certificate."
msgstr "Sertifika dışa aktarılamadı."

#: ui/gcr-certificate-renderer.c:527 ui/gcr-certificate-request-renderer.c:309
msgid "Identity"
msgstr "Kimlik"

#: ui/gcr-certificate-renderer.c:531
msgid "Verified by"
msgstr "Doğrulayan"

#: ui/gcr-certificate-renderer.c:538 ui/gcr-gnupg-renderer.c:719
msgid "Expires"
msgstr "Bitiş Tarihi"

#. The subject
#: ui/gcr-certificate-renderer.c:545 ui/gcr-certificate-request-renderer.c:315
msgid "Subject Name"
msgstr "Konu Adı"

#. The Issuer
#: ui/gcr-certificate-renderer.c:550
msgid "Issuer Name"
msgstr "Veren Adı"

#. The Issued Parameters
#: ui/gcr-certificate-renderer.c:555
msgid "Issued Certificate"
msgstr "Verilen Sertifika"

#: ui/gcr-certificate-renderer.c:560 ui/gcr-certificate-request-renderer.c:326
msgid "Version"
msgstr "Sürüm"

#: ui/gcr-certificate-renderer.c:574
msgid "Not Valid Before"
msgstr "Öncesi Geçerli Değil"

#: ui/gcr-certificate-renderer.c:579
msgid "Not Valid After"
msgstr "Sonrası Geçerli Değil"

#. Fingerprints
#: ui/gcr-certificate-renderer.c:584
msgid "Certificate Fingerprints"
msgstr "Sertifika Parmak İzleri"

#. Public Key Info
#: ui/gcr-certificate-renderer.c:590 ui/gcr-certificate-request-renderer.c:329
#: ui/gcr-certificate-request-renderer.c:375
msgid "Public Key Info"
msgstr "Genel Anahtar Bilgisi"

#. Signature
#: ui/gcr-certificate-renderer.c:605 ui/gcr-certificate-renderer.c:915
#: ui/gcr-certificate-request-renderer.c:345
#: ui/gcr-certificate-request-renderer.c:382 ui/gcr-gnupg-renderer.c:560
msgid "Signature"
msgstr "İmza"

#: ui/gcr-certificate-renderer.c:622
msgid "Export Certificate…"
msgstr "Sertifikayı Dışa Aktar…"

#: ui/gcr-certificate-renderer.c:861
msgid "Key Algorithm"
msgstr "Anahtar Algoritması"

#: ui/gcr-certificate-renderer.c:866
msgid "Key Parameters"
msgstr "Anahtar Parametreleri"

#: ui/gcr-certificate-renderer.c:874 ui/gcr-gnupg-renderer.c:353
msgid "Key Size"
msgstr "Anahtar Boyutu"

#: ui/gcr-certificate-renderer.c:882
msgid "Key SHA1 Fingerprint"
msgstr "Anahtar SHA1 Parmak İzi"

#: ui/gcr-certificate-renderer.c:904
msgid "Signature Algorithm"
msgstr "İmza Algoritması"

#: ui/gcr-certificate-renderer.c:908
msgid "Signature Parameters"
msgstr "İmza Parametreleri"

#: ui/gcr-certificate-renderer.c:957
msgid "Critical"
msgstr "Kritik"

#. The certificate request type
#: ui/gcr-certificate-request-renderer.c:95
#: ui/gcr-certificate-request-renderer.c:304
#: ui/gcr-certificate-request-renderer.c:319
#: ui/gcr-certificate-request-renderer.c:362
#: ui/gcr-certificate-request-renderer.c:367
msgid "Certificate request"
msgstr "Sertifika istemi"

#: ui/gcr-certificate-request-renderer.c:257
msgid "Attribute"
msgstr "Öznitelik"

#: ui/gcr-certificate-request-renderer.c:261
#: ui/gcr-certificate-request-renderer.c:320
#: ui/gcr-certificate-request-renderer.c:368 ui/gcr-gnupg-renderer.c:591
#: ui/gcr-gnupg-renderer.c:593
msgid "Type"
msgstr "Tür"

#: ui/gcr-certificate-request-renderer.c:372
msgid "Challenge"
msgstr "Kimlik Sorma"

#: ui/gcr-display-view.c:298
msgid "_Details"
msgstr "_Ayrıntılar"

#: ui/gcr-failure-renderer.c:159
#, c-format
msgid "Could not display “%s”"
msgstr "“%s” gösterilemedi"

#: ui/gcr-failure-renderer.c:161
msgid "Could not display file"
msgstr "Dosya gösterilemedi"

#: ui/gcr-failure-renderer.c:166
msgid "Reason"
msgstr "Neden"

#: ui/gcr-failure-renderer.c:216
#, c-format
msgid "Cannot display a file of this type."
msgstr "Bu türde dosya gösterilemiyor."

#: ui/gcr-gnupg-renderer.c:203
msgid "Elgamal"
msgstr "Elgamal"

#: ui/gcr-gnupg-renderer.c:216
msgid "Encrypt"
msgstr "Şifrele"

#: ui/gcr-gnupg-renderer.c:218
msgid "Sign"
msgstr "İmzala"

#: ui/gcr-gnupg-renderer.c:220
msgid "Certify"
msgstr "Onayla"

#: ui/gcr-gnupg-renderer.c:222
msgid "Authenticate"
msgstr "Kimlik Doğrula"

#: ui/gcr-gnupg-renderer.c:224
msgctxt "capability"
msgid "Disabled"
msgstr "Devre Dışı"

#: ui/gcr-gnupg-renderer.c:255 ui/gcr-gnupg-renderer.c:414
#: ui/gcr-key-renderer.c:391 ui/gcr-key-renderer.c:395
msgid "Unknown"
msgstr "Bilinmeyen"

#: ui/gcr-gnupg-renderer.c:257
msgid "Invalid"
msgstr "Geçersiz"

#: ui/gcr-gnupg-renderer.c:259
msgctxt "ownertrust"
msgid "Disabled"
msgstr "Devre Dışı"

#: ui/gcr-gnupg-renderer.c:261
msgid "Revoked"
msgstr "Feshedilmiş"

#: ui/gcr-gnupg-renderer.c:263
msgid "Expired"
msgstr "Süresi Dolmuş"

#: ui/gcr-gnupg-renderer.c:265
msgid "Undefined trust"
msgstr "Tanımsız güven"

#: ui/gcr-gnupg-renderer.c:267
msgid "Distrusted"
msgstr "Güvenilmez"

#: ui/gcr-gnupg-renderer.c:269
msgid "Marginally trusted"
msgstr "Kısmi güvenilir"

#: ui/gcr-gnupg-renderer.c:271
msgid "Fully trusted"
msgstr "Tümüyle güvenilir"

#: ui/gcr-gnupg-renderer.c:273
msgid "Ultimately trusted"
msgstr "Sınırsız güvenilir"

#: ui/gcr-gnupg-renderer.c:287
msgid "The information in this key has not yet been verified"
msgstr "Bu anahtardaki bilgi henüz doğrulanmamıştır"

#: ui/gcr-gnupg-renderer.c:290
msgid "This key is invalid"
msgstr "Anahtar geçersiz"

#: ui/gcr-gnupg-renderer.c:293
msgid "This key has been disabled"
msgstr "Anahtar devre dışı bırakıldı"

#: ui/gcr-gnupg-renderer.c:296
msgid "This key has been revoked"
msgstr "Anahtar iptal edildi"

#: ui/gcr-gnupg-renderer.c:299
msgid "This key has expired"
msgstr "Bu anahtarın süresi doldu"

#: ui/gcr-gnupg-renderer.c:304
msgid "This key is distrusted"
msgstr "Bu anahtar güvenilir anahtar değildir"

#: ui/gcr-gnupg-renderer.c:307
msgid "This key is marginally trusted"
msgstr "Bu anahtar kısmi güvenilir anahtardır"

#: ui/gcr-gnupg-renderer.c:310
msgid "This key is fully trusted"
msgstr "Bu anahtar tam güvenilir anahtardır"

#: ui/gcr-gnupg-renderer.c:313
msgid "This key is ultimately trusted"
msgstr "Bu anahtar sınırsız güvenilir anahtardır"

#: ui/gcr-gnupg-renderer.c:338 ui/gcr-gnupg-renderer.c:564
msgid "Key ID"
msgstr "Anahtar Kimliği"

#: ui/gcr-gnupg-renderer.c:346 ui/gcr-gnupg-renderer.c:572
#: ui/gcr-gnupg-renderer.c:619 ui/gcr-key-renderer.c:392
msgid "Algorithm"
msgstr "Algoritma"

#: ui/gcr-gnupg-renderer.c:361 ui/gcr-gnupg-renderer.c:438
#: ui/gcr-gnupg-renderer.c:481
msgid "Created"
msgstr "Oluşturulma"

#: ui/gcr-gnupg-renderer.c:370 ui/gcr-gnupg-renderer.c:447
#: ui/gcr-gnupg-renderer.c:490
msgid "Expiry"
msgstr "Bitiş"

#: ui/gcr-gnupg-renderer.c:379
msgid "Capabilities"
msgstr "Yetenekler"

#: ui/gcr-gnupg-renderer.c:392
msgid "Owner trust"
msgstr "Sahip güvenilirliği"

#: ui/gcr-gnupg-renderer.c:420
msgid "Name"
msgstr "Ad"

#: ui/gcr-gnupg-renderer.c:426 ui/gcr-gnupg-renderer.c:708
msgid "Comment"
msgstr "Yorum"

#: ui/gcr-gnupg-renderer.c:466
msgid "User Attribute"
msgstr "Kullanıcı Özniteliği"

#: ui/gcr-gnupg-renderer.c:473 ui/gcr-key-renderer.c:398
msgid "Size"
msgstr "Boyut"

#: ui/gcr-gnupg-renderer.c:508
msgid "Signature of a binary document"
msgstr "İkili belgenin imzası"

#: ui/gcr-gnupg-renderer.c:510
msgid "Signature of a canonical text document"
msgstr "Standart metin belgesi imzası"

#: ui/gcr-gnupg-renderer.c:512
msgid "Standalone signature"
msgstr "Bağımsız imza"

#: ui/gcr-gnupg-renderer.c:514
msgid "Generic certification of key"
msgstr "Genel anahtar sertifikası"

#: ui/gcr-gnupg-renderer.c:516
msgid "Persona certification of key"
msgstr "Kişisel anahtar sertifikası"

#: ui/gcr-gnupg-renderer.c:518
msgid "Casual certification of key"
msgstr "Anahtar için rastgele sertifika"

#: ui/gcr-gnupg-renderer.c:520
msgid "Positive certification of key"
msgstr "Olumlu anahtar belgelendirme"

#: ui/gcr-gnupg-renderer.c:522
msgid "Subkey binding signature"
msgstr "Alt anahtar bağlama imzası"

#: ui/gcr-gnupg-renderer.c:524
msgid "Primary key binding signature"
msgstr "Birincil anahtar bağlama imzası"

#: ui/gcr-gnupg-renderer.c:526
msgid "Signature directly on key"
msgstr "Anahtar üzerindeki doğrudan imza"

#: ui/gcr-gnupg-renderer.c:528
msgid "Key revocation signature"
msgstr "Anahtar iptal imzası"

#: ui/gcr-gnupg-renderer.c:530
msgid "Subkey revocation signature"
msgstr "Alt Anahtar iptal imzası"

#: ui/gcr-gnupg-renderer.c:532
msgid "Certification revocation signature"
msgstr "Sertifika iptal imzası"

#: ui/gcr-gnupg-renderer.c:534
msgid "Timestamp signature"
msgstr "Zaman damgası imzası"

#: ui/gcr-gnupg-renderer.c:536
msgid "Third-party confirmation signature"
msgstr "Üçüncü parti onaylama imzası"

#: ui/gcr-gnupg-renderer.c:589 ui/gcr-gnupg-renderer.c:597
msgid "Class"
msgstr "Sınıf"

#: ui/gcr-gnupg-renderer.c:591
msgid "Local only"
msgstr "Yalnızca yerel"

#: ui/gcr-gnupg-renderer.c:593
msgid "Exportable"
msgstr "Dışa aktarılabilir"

#: ui/gcr-gnupg-renderer.c:611
msgid "Revocation Key"
msgstr "İptal Anahtarı"

#: ui/gcr-gnupg-renderer.c:625 ui/gcr-gnupg-renderer.c:649
#: ui/gcr-gnupg-renderer.c:651
msgid "Fingerprint"
msgstr "Parmak izi"

#: ui/gcr-gnupg-renderer.c:740
msgid "Public Subkey"
msgstr "Açık Alt Anahtar"

#: ui/gcr-gnupg-renderer.c:742
msgid "Secret Key"
msgstr "Gizli Anahtar"

#: ui/gcr-gnupg-renderer.c:744
msgid "Secret Subkey"
msgstr "Gizli Alt Anahtar"

#: ui/gcr-import-button.c:118
msgid "Initializing…"
msgstr "Başlatılıyor…"

#: ui/gcr-import-button.c:126
msgid "Import is in progress…"
msgstr "İçe aktarma işlemi devam ediyor…"

#: ui/gcr-import-button.c:133
#, c-format
msgid "Imported to: %s"
msgstr "İçe aktarıldı: %s"

#: ui/gcr-import-button.c:153
#, c-format
msgid "Import to: %s"
msgstr "İçe aktar: %s"

#: ui/gcr-import-button.c:166
msgid "Cannot import because there are no compatible importers"
msgstr "İçe aktarma işlemi uyumlu içe aktarıcı olmadığından yapılamıyor"

#: ui/gcr-import-button.c:175
msgid "No data to import"
msgstr "İçe aktarılacak veri yok"

#: ui/gcr-key-renderer.c:89
msgid "Key"
msgstr "Anahtar"

#: ui/gcr-key-renderer.c:355
msgid "Private RSA Key"
msgstr "RSA Gizli Anahtarı"

#: ui/gcr-key-renderer.c:357
msgid "Private DSA Key"
msgstr "DSA Gizli Anahtarı"

#: ui/gcr-key-renderer.c:359
msgid "Private Elliptic Curve Key"
msgstr "Özel Eliptik Eğri Anahtarı"

#: ui/gcr-key-renderer.c:364 ui/gcr-key-renderer.c:366
msgid "Public DSA Key"
msgstr "DSA Genel Anahtarı"

#: ui/gcr-key-renderer.c:368
msgid "Public Elliptic Curve Key"
msgstr "Genel Eliptik Eğri Anahtarı"

#: ui/gcr-key-renderer.c:377
#, c-format
msgid "%u bit"
msgid_plural "%u bits"
msgstr[0] "%u bit"

#: ui/gcr-key-renderer.c:378
msgid "Strength"
msgstr "Güç"

#. Fingerprints
#: ui/gcr-key-renderer.c:402
msgid "Fingerprints"
msgstr "Parmak izleri"

#: ui/gcr-key-renderer.c:406
msgid "SHA1"
msgstr "SHA1"

#: ui/gcr-key-renderer.c:411
msgid "SHA256"
msgstr "SHA256"

#. Add our various buttons
#: ui/gcr-pkcs11-import-dialog.c:104 ui/gcr-prompt-dialog.c:605
#: ui/gcr-certificate-exporter.c:229 ui/gcr-certificate-exporter.c:306
msgid "_Cancel"
msgstr "_Vazgeç"

#: ui/gcr-pkcs11-import-dialog.c:106 ui/gcr-prompt-dialog.c:608
msgid "_OK"
msgstr "_Tamam"

#: ui/gcr-pkcs11-import-dialog.c:179
msgid "Automatically chosen"
msgstr "Kendiliğinden seçilmiş"

#: ui/gcr-pkcs11-import-dialog.c:263 ui/gcr-pkcs11-import-interaction.c:142
#: ui/gcr-pkcs11-import-interaction.c:161
#, c-format
msgid "The user cancelled the operation"
msgstr "Kullanıcı işlemi iptal etti"

#: ui/gcr-pkcs11-import-dialog.ui:31
msgid "In order to import, please enter the password."
msgstr "Lütfen içe aktarmak için parola girin."

#. The password label
#: ui/gcr-pkcs11-import-dialog.ui:66 ui/gcr-prompt-dialog.c:666
msgid "Password:"
msgstr "Parola:"

#: ui/gcr-pkcs11-import-dialog.ui:80
msgid "Token:"
msgstr "Jeton:"

#: ui/gcr-pkcs11-import-dialog.ui:178
msgid "Label:"
msgstr "Etiket:"

#: ui/gcr-pkcs11-import-dialog.ui:233
msgid "Import settings"
msgstr "İçe Aktarma ayarları"

#. The confirm label
#: ui/gcr-prompt-dialog.c:683
msgid "Confirm:"
msgstr "Onayla:"

#: ui/gcr-prompt-dialog.c:751
msgid "Passwords do not match."
msgstr "Parolalar eşleşmiyor."

#: ui/gcr-prompt-dialog.c:758
msgid "Password cannot be blank"
msgstr "Parola boş bırakılamaz"

#: ui/gcr-prompter.desktop.in.in:3
msgid "Access Prompt"
msgstr "Erişim Ekranı"

#: ui/gcr-prompter.desktop.in.in:4
msgid "Unlock access to passwords and other secrets"
msgstr "Parolalar ve diğer gizli bilgiler için erişim kilidini aç"

#: ui/gcr-certificate-exporter.c:226
msgid "A file already exists with this name."
msgstr "Bu adda dosya zaten var."

#: ui/gcr-certificate-exporter.c:227
msgid "Do you want to replace it with a new file?"
msgstr "Yeni dosya ile değiştirmek istiyor musunuz?"

#: ui/gcr-certificate-exporter.c:230
msgid "_Replace"
msgstr "_Değiştir"

#: ui/gcr-certificate-exporter.c:260
#, c-format
msgid "The operation was cancelled."
msgstr "İşlem iptal edildi."

#: ui/gcr-certificate-exporter.c:304
msgid "Export certificate"
msgstr "Sertifikayı Dışa Aktar"

#: ui/gcr-certificate-exporter.c:307
msgid "_Save"
msgstr "_Kaydet"

#: ui/gcr-certificate-exporter.c:316
msgid "Certificate files"
msgstr "Sertifika dosyaları"

#: ui/gcr-certificate-exporter.c:327
msgid "PEM files"
msgstr "PEM dosyaları"

#: ui/gcr-unlock-options-widget.ui:16
msgid "Automatically unlock this keyring whenever I’m logged in"
msgstr "Her oturum açtığımda, bu anahtarlığın kilidini kendiliğinden aç"

#: ui/gcr-unlock-options-widget.ui:31
msgid "Lock this keyring when I log out"
msgstr "Oturumu kapattığımda bu anahtarlığı kilitle"

#: ui/gcr-unlock-options-widget.ui:53
msgid "Lock this keyring after"
msgstr "Bu anahtarlığı şu kadar sonra kilitle"

#: ui/gcr-unlock-options-widget.ui:67
msgid "Lock this keyring if idle for"
msgstr "Bu anahtarlığı şu kadar süre boştaysa kilitle"

#. Translators: The 'minutes' from 'Lock this keyring if idle for x minutes'.
#: ui/gcr-unlock-options-widget.ui:103
msgid "minutes"
msgstr "dakika"

#: ui/gcr-unlock-renderer.c:68
#, c-format
msgid "Unlock: %s"
msgstr "Kilidi aç: %s"

#: ui/gcr-unlock-renderer.c:122
msgid "Password"
msgstr "Parola"

#: ui/gcr-unlock-renderer.c:274
#, c-format
msgid ""
"The contents of “%s” are locked. In order to view the contents, enter the "
"correct password."
msgstr ""
"“%s” içeriği kilitlendi. İçeriği görüntülemek için doğru parolayı girin."

#: ui/gcr-unlock-renderer.c:277
msgid ""
"The contents are locked. In order to view the contents, enter the correct "
"password."
msgstr "İçerikler kilitlendi. İçeriği görüntülemek için doğru parolayı girin."

#: ui/gcr-viewer.desktop.in.in:3
msgid "View file"
msgstr "Dosyayı görüntüle"

#: ui/gcr-viewer-tool.c:40
msgid "GCR Certificate and Key Viewer"
msgstr "GCR Sertifika ve Anahtar Görüntüleyici"

#: ui/gcr-viewer-tool.c:47
msgid "Show the application's version"
msgstr "Uygulamanın sürümünü göster"

#: ui/gcr-viewer-tool.c:49
msgid "[file...]"
msgstr "[dosya...]"

#: ui/gcr-viewer-tool.c:100
msgid "- View certificate and key files"
msgstr "- Sertifika ve anahtar dosyaların görüntüle"

#: ui/gcr-viewer-tool.c:114 ui/gcr-viewer-widget.c:669
msgid "Certificate Viewer"
msgstr "Sertifika Görüntüleyici"

#: ui/gcr-viewer-widget.c:189
msgid "The password was incorrect"
msgstr "Parola hatalı"

#: ui/gcr-viewer-window.c:74
msgid "Imported"
msgstr "İçe aktarıldı"

#: ui/gcr-viewer-window.c:78
msgid "Import failed"
msgstr "İçe aktarma başarısız oldu"

#: ui/gcr-viewer-window.c:105
msgid "Import"
msgstr "İçe Aktar"

#: ui/gcr-viewer-window.c:114
msgid "_Close"
msgstr "_Kapat"

#, fuzzy
#~ msgid "Couldn't setup PKCS#11 module: %s"
#~ msgstr "Dosya silinemedi: %s"

#, fuzzy
#~ msgid "<b>Unlock</b>"
#~ msgstr "Kilidini kaldır"
